#!/bin/zsh

ROOT=`pwd`
MINIFIED='NO'

# dojo build profiles
BUILD_SCRIPTS_DIR="${ROOT}/util/buildscripts"
BUILD_PROFILES_DIR="${BUILD_SCRIPTS_DIR}/profiles"
APP_PROFILE="${ROOT}/build.profile.js"
BUILD_PROFILE="${BUILD_PROFILES_DIR}/build.profile.js"
BUILD_DIR="${ROOT}/build"
STATIC_DIR="${ROOT}/static/site/js"


function print () {
    printf "[DOJO BUILD] %s\n" "$1"
}


function rm_file () {
    if [ -e "$1" ]; then
        rm -f "$1"
    fi
}


function rm_dir () {
    if [ -d "$1" ]; then
        rm -rf "$1"
    fi
}


function build_dojo () {
    print "copy build profile to buildscripts dir"
    cp "${APP_PROFILE}" "${BUILD_PROFILE}"

    # Relocate ourselves to the buildscript directory
    print "cd to the script dirctory..."
    cd "${BUILD_SCRIPTS_DIR}"

    # run our build
    # this dumps everything into the build/site/js directory
    # XXX: check for node???
    print "run the build..."
    print "***********************************************"
    node --debug "${ROOT}/dojo/dojo.js" load=build profile=build action=release
}

function clean_dojo_build () {
    print "cleanup old dojo build..."
    rm_file "${BUILD_PROFILE}"
    rm_dir "${BUILD_DIR}"
    rm_dir "${STATIC_DIR}"
    mkdir -p $STATIC_DIR
}

function relocate_to_static () {
    print "moving build files to static..."
    cp -R "${BUILD_DIR}/" $STATIC_DIR
    rm "${STATIC_DIR}/build-report.txt"
}

function output_development () {
    ln -s "${ROOT}/dojo" "${STATIC_DIR}/dojo"
    ln -s "${ROOT}/dojox" "${STATIC_DIR}/dojox"
    ln -s "${ROOT}/dijit" "${STATIC_DIR}/dijit"
    ln -s "${ROOT}/kinsey" "${STATIC_DIR}/kinsey"
    ln -s "${ROOT}/mustache" "${STATIC_DIR}/mustache"
    ln -s "${ROOT}/app" "${STATIC_DIR}/app"
}

function output_minified () {
    build_dojo
    print_line
    print "strip out all non-minified javascript files..."
    find $BUILD_DIR -type f -name "*.uncompressed.js" -exec rm {} \;
    find $BUILD_DIR -type f -name "*.consoleStripped.js" -exec rm {} \;
    relocate_to_static
}

function output_normal () {
    build_dojo
    print_line
    # remove everything but the uncompressed files
    # this will include normal as well as consoleStripped files
    print "strip out all minified and console stripped files..."
    find $BUILD_DIR -type f -name "*.js" -and \! -name "*.uncompressed.js" -exec rm {} \;
    # rename the uncompressed files to regular
    print "rename all uncompressed files..."
    find $BUILD_DIR  -type f -name "*.uncompressed.js" | awk '{print("mv "$1" "$1)}' | sed s/\.uncompressed\.js//2 | /bin/sh
    relocate_to_static
}

function print_line () {
    print "***********************************************"
}

print "RUN DOJO BUILD"
print_line
clean_dojo_build
print_line

if [[ "$1" == "--compressed" ]]; then
    print "created compressed output..."
    output_minified
elif [[ "$1" == "--development" ]]; then
    print "create symlinked output for development..."
    output_development
else
    print "create normal output..."
    output_normal
fi

cd "${ROOT}"
print "***********************************************"
print "dojo build complete!"

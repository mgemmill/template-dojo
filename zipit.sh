ZIPPED="template-dojo.tar.gz"

if [ -e "${ZIPPED}" ]; then
    rm $ZIPPED;
fi

tar cvzf $ZIPPED ./build.profile.js ./build.sh ./getDojo.sh ./rmDojo.sh ./app ./.gitignore

//>> pure-amd
define([
    'dojo',
    'dojo/string',
    'util/doh/main', 
    'app/tools'
], function (
    dojo,
    string,
    doh,
    tools
    ) {
    
    var sub = string.substitute;

    console.debug("defining test_sample");

    doh.register( "app.tools", [
        { 
            name: "hello test",
            runTest: function() {
                doh.is("Hello World!", tools.hello(), "");
            }
        }
    ]);

});


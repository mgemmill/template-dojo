//>> pure-amd
define([
    'dojo', 
    'dojo/string'
], function(
    dojo,
    string
    ) {

    return {
        hello: function() {
            return "Hello World!";
        }
    };

});

var profile = (function (deployment) {
    return {
        layerOptimize: "closure",
        optimize:"closure",
        stripConsole: (function() {
            /* none,normal,warn,all */
            if ( deployment === "prod") {
                return "all";
            } else if ( deployment === "dev" ) {
                return "none";
            } else {
                return "normal";
            }
        })(), 
        mini:"true",
        action:"clean,release",
        cssOptimize:"comments.keepLines",
        releaseDir:"../../../build",
        releaseName:"",
        selectorEngine:"acme",

        packages: [
            { name: "dojo",
              location: "../../../dojo"
            },
            { name: "dojox",
              location: "../../../dojox"
            },
            { name: "dijit",
              location: "../../../dijit"
            },
            { name: "app",
              location: "../../../app"
            }

        ],

        layers: {
            "dojo": {
                include: [ 
                    "dojo/request",
                    "dojo/has",
                    "dojo/_base/kernel",
                    "dojo/promise/first",
                    "dojo/fx/Toggler",
                    "dojox/lang/functional/lambda",
                    "dojox/lang/functional/object",
                    "dijit/_base",
                    "dijit/form/ComboButton",
                    "dijit/form/ToggleButton",
                    "dijit/form/DateTextBox",
                    "kinsey/util",
                    "app" 
                    ],
                exclude: [
                    ]
            }
        },

        staticHasFeatures: {
            //"config-dojo-loader-catches": 0,
            //"config-tlmSiblingOfDojo": 0,
            //"dojo-amd-factory-scan": 0,
            //"dojo-combo-api": 0,
            //"dojo-config-api": 1,
            //"dojo-config-require": 0,
            //"dojo-debug-messages": 0,
            //"dojo-dom-ready-api": 1,
            //"dojo-firebug": 0,
            //"dojo-guarantee-console": 1,
            //"dojo-has-api": 1,
            //"dojo-inject-api": 1,
            //"dojo-loader": 1,
            //"dojo-log-api": 0,
            //"dojo-modulePaths": 0,
            //"dojo-moduleUrl": 0,
            //"dojo-publish-privates": 0,
            //"dojo-requirejs-api": 0,
            //"dojo-sniff": 0,
            //"dojo-sync-loader": 0,
            //"dojo-test-sniff": 0,
            //"dojo-timeout-api": 0,
            //"dojo-trace-api": 0,
            //"dojo-undef-api": 0,
            //"dojo-v1x-i18n-Api": 1,
            //"dom": 1,
            //"host-browser": 1,
            //"extend-dojo": 1
        }
    };

})("dev");

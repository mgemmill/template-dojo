#!/bin/bash

function install_dojo() {
    # Download the archive as a tar.gz. You could just as easily get the zip file
    if [ ! -f "$DOJOTAR" ]
        then    
            wget "http://download.dojotoolkit.org/release-$RELEASE/$DOJOTAR"
    fi

    # Unpack the archive
    echo "unpack $DOJOTAR"
    tar -xf "$DOJOTAR"

    # Move the contents of the newly unpacked folder into this folder
    echo "move contents of $DOJOSRC to current directory"
    # why will this command not work from here?
    mv "$DOJOSRC"/* ./

    # Remove delete the archive and the now empty folder it contained
    echo "remove src files and directories"
    rm -rf "$DOJOSRC"
    #rm "$DOJOTAR"
}

echo "$1"
if [ "$1" = "" ] 
  then
    echo "You must supply a dojo version number."
  else
    echo "OK"
    RELEASE="$1"
    DOJOSRC="dojo-release-$RELEASE-src"
    DOJOTAR="$DOJOSRC.tar.gz"
    install_dojo
fi
